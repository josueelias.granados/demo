<?php
echo "Hola";
?>
<html>
<head></head>
<title></title>
<body>

<table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                                <tr>
                                                    <th class="alineadocenter">ID</th>
													<th class="alineadocenter">PAIS</th>
                                                    <th class="alineadocenter">CODIGO INTERNO</th>
                                                    <th class="alineadocenter">CODIGO ISO</th>
													<th class="alineadocenter">ACCIONES</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
												
													include_once './db.php';

													$db = new DatabaseClass();
													$data = $db->Select("Select * from pais");
													
													foreach($data as $result)
													{
														echo '
														<tr class="gradeX">
															<td class="alineadocenter">' . $result['id'] . '</td>
															<td class="alineadocenter">' . $result['nombre'] . '</td>
															<td class="alineadocenter">' . $result['codigoInterno'] . '</td>
															<td class="alineadocenter">' . $result['codigoISO'] . '</td>
															<td class="alineadocenter">
																<a href="index.php?accion=ver&ID=' . $result['id'] . '"><i class="icon-pencil"></i> Actualizar</a>
																<br>
																<a onclick="return confirm(\'¿Esta seguro de eliminar a : ' . $result['nombre'] . '? \')" href="index.php?accion=eliminar&ID=' . $result['id'] . '"><i class="icon-trash"></i> Eliminar</a>
															</td>
														</tr>';
													}
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="alineadocenter">ID</th>
                                                    <th class="alineadocenter">PAIS</th>
                                                    <th class="alineadocenter">CODIGO INTERNO</th>
                                                    <th class="alineadocenter">CODIGO ISO</th>
													<th class="alineadocenter">ACCIONES</th>
                                                </tr>
                                            </tfoot>
                                        </table>


</body>
</html>
